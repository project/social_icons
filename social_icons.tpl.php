<!-- Social Icons Template-->
<div class="background">
    <div class="contacts social">
        <h3><span class="title"><?php print variable_get('contact_info_title'); ?></span></h3>
        <div class="dark">                                     
            <?php if($settings['facebook']): ?>
            <a href="<?php print $settings['facebook'];?>" title="Facebook"><i class="fa fa-facebook-square"></i></a>
            <?php endif;?>
            <?php if($settings['twitter']): ?>
            <a href="<?php print $settings['twitter'];?>" title="Twitter"><i class="fa fa-twitter-square"></i></a>
            <?php endif;?>
          
            <?php if($settings['googleplus']): ?>
            <a href="<?php print $settings['googleplus'];?>" title="Google Plus"><i class="fa fa-google-plus-square"></i></a>
            <?php endif;?>
			
			<?php if($settings['instagram']): ?>
            <a href="<?php print $settings['instagram'];?>" title="Instagram"><i class="fa fa-instagram"></i></a>
            <?php endif;?>
			
			<?php if($settings['linkedin']): ?>
            <a href="<?php print $settings['linkedin'];?>" title="LinkedIn"><i class="fa fa-linkedin-square"></i></a>
            <?php endif;?>

			<?php if($settings['pinterest']): ?>
            <a href="<?php print $settings['pinterest'];?>" title="Pinterest"><i class="fa fa-pinterest-square"></i></a>
            <?php endif;?>


			<?php if($settings['youtube']): ?>
            <a href="<?php print $settings['youtube'];?>" title="Youtube"><i class="fa fa-youtube-square"></i></a>
            <?php endif;?>
            
        </div>
    </div>
</div>
<!-- End Social Icons-->